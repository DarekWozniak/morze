def code_morze(value):
    """
    Encode message in Morse code
    Parameters
    ----------
    value: str
        Message to be encoded
    Returns
    -------
    Morze encoded string.
    """
    morse_dictionary = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..',
                        'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
                        'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..',
                        'M': '--', 'N': '-.', 'O': '---', 'P': '.--.',
                        'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
                        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
                        'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---',
                        '3': '...--', '4': '....-', '5': '.....', '6': '-....',
                        '7': '--...', '8': '---..', '9': '----.', '0': '-----',
                        ', ': '--..--', '.': '.-.-.-', '?': '..--..',
                        '/': '-..-.', '-': '-....-', '(': '-.--.',
                        ')': '-.--.-'}
    try:
        value = value.replace(" ", "").upper()
    except AttributeError:
        raise AttributeError(f"Invalid entry: {value}. Possibly not string.")
    encoded = ''.join(morse_dictionary[letter] + " " for letter in value)
    encoded = encoded.strip()
    return encoded
